﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Booking_a_Reservation_to_a_Hotel.aspx.cs" Inherits="N01314922_Assignment1.Booking_a_Reservation_to_a_Hotel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Reservation</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p> <b> HOTEL ROOM RESERVATION </b></p>

            <asp:validationsummary forecolor="Red" runat="server" id="validationSummary"> </asp:validationsummary>
            <br />

            <label><b>First Name:</b></label> <asp:TextBox runat="server" ID="customerfName" placeholder=" First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Fisrt Name" forecolor="Red" ControlToValidate="customerfName" ID="validatorfName"></asp:RequiredFieldValidator>
            <br />
            <label><b>Last Name:</b></label> <asp:TextBox runat="server" ID="customerlName" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Last Name" forecolor="Red" ControlToValidate="customerlName" ID="validatorlName"></asp:RequiredFieldValidator>
            <br />


            <label><b>Phone:</b></label> <asp:TextBox runat="server" ID="customerPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" forecolor="Red" ControlToValidate="customerPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="customerPhone" Type="String" Operator="NotEqual" ValueToCompare="123456789" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />

            <label><b>Email:</b></label> <asp:TextBox runat="server" ID="customerEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" forecolor="Red" ControlToValidate="customerEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="customerEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            
            <label><b>Address:</b></label> <asp:TextBox runat="server" ID="customerAddress" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Address" forecolor="Red" ControlToValidate="customerAddress" ID="validatorAddress"></asp:RequiredFieldValidator>
            <br />

            <label><b>checkin Time:</b></label><br />
            <asp:DropDownList runat="server" ID="checkinTime" placeholder="Checkin Time (24hour format)">
                <asp:ListItem Value="1100" Text="11:00am"> </asp:ListItem>
                <asp:ListItem Value="1200" Text="12:00pm"> </asp:ListItem>
                <asp:ListItem Value="1300" Text="1:00pm">  </asp:ListItem>
                <asp:ListItem Value="1400" Text="2:00pm">  </asp:ListItem>
                <asp:ListItem Value="1500" Text="3:00pm">  </asp:ListItem>
                <asp:ListItem Value="1600" Text="4:00pm">  </asp:ListItem>
                <asp:ListItem Value="1700" Text="5:00pm">  </asp:ListItem>
                <asp:ListItem Value="1800" Text="6:00pm">  </asp:ListItem>
                <asp:ListItem Value="1900" Text="7:00pm">  </asp:ListItem>
                <asp:ListItem Value="2000" Text="8:00pm">  </asp:ListItem>
            </asp:DropDownList>
            <br />

               <label><b>How many Pesrsons:</b></label> <asp:DropDownList runat="server" ID="numberofCustomer" Type="Integer">
                <asp:ListItem Value="One" Text="1"></asp:ListItem>
                <asp:ListItem Value="Two" Text="2"></asp:ListItem>
                <asp:ListItem Value="Three" Text="3"></asp:ListItem>
                <asp:ListItem Value="Four" Text="4"></asp:ListItem>
            </asp:DropDownList>
            <br />

             <label><b>Type of Room:</b></label> <asp:DropDownList runat="server" ID="typeofRoom">
                <asp:ListItem Value="S" Text="Small"></asp:ListItem>
                <asp:ListItem Value="M" Text="Medium"></asp:ListItem>
                <asp:ListItem Value="D" Text="Deluxe"></asp:ListItem>
            </asp:DropDownList>
            <br />

            <label><b>Additional Service:</b></label>
            <div id="service" runat="server">
            <asp:CheckBox runat="server" ID="additionalService1" Text="Food" /><br />
            <asp:CheckBox runat="server" ID="additionalService2" Text="Room Decoration" /><br />
            <asp:CheckBox runat="server" ID="additionalService3" Text="Car" /><br />
            <asp:CheckBox runat="server" ID="additionalService4" Text="Guide" /><br />
            </div>
            <br />

            <label><b>Type of Payment</b></label> <br />
            <asp:RadioButton runat="server" Text="Debit Card" GroupName="payment"/><br />
            <asp:RadioButton runat="server" Text="Credit Card" GroupName="payment"/><br />
            <asp:RadioButton runat="server" Text="Online Banking" GroupName="payment"/><br />
            <asp:RadioButton runat="server" Text="Cash" GroupName="payment"/>
            <br />

            <asp:Button runat="server" ID="myButton" onclick="Conformation" Text="Submit"/>
            <br />
            
            <div runat="server" id="Customerref"></div>

            <div runat="server" id="Servicesref"></div>

            <div runat="server" id="Conformationref"></div>

           

            </div>
    </form>
</body>
</html>
