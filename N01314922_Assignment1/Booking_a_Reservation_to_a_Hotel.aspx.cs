﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace N01314922_Assignment1
{
    public partial class Booking_a_Reservation_to_a_Hotel : System.Web.UI.Page
    {
        private object additionalService;

        protected void Page_Load(object sender, EventArgs e)
        {
         

            // creating customer object

            string name = customerfName.Text.ToString();
            string phone = customerPhone.Text.ToString();
            string email = customerEmail.Text.ToString();
            string address = customerAddress.Text.ToString();
            Customer newcustomer = new Customer();
            newcustomer.CustomerName = name;
            newcustomer.CustomerPhone = phone;
            newcustomer.CustomerEmail = email;
            newcustomer.CustomerAddress = address;


            //Creating the Services object

            int customerNo = int.Parse(numberofCustomer.Text);
            string room = typeofRoom.SelectedItem.Value.ToString();
            List<string> additionalservices = new List<string> { "Room Cleaning","Dinner" };
            string today = DateTime.Today.ToString("dd-MM-yyyy");
            string time = checkinTime.Text.ToString();
            string hour = time.Substring(0, 2);
            string minute = time.Substring(time.Length - 2);
            string trydate = today + " " + hour + ":" + minute;
            DateTime checkintime = DateTime.ParseExact(trydate, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
            Services newservices = new Services(customerNo, room, additionalservices, checkintime);



        }
    }
}