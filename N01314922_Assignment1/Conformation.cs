﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace N01314922_Assignment1
{
    public class Conformation
    {
        public Customer customer;
        public Services Services;

        public Conformation (Customer c, Services s)
        {
            customer = c;
            Services = s;
        }


        public string PrintSummary()
        {
            string summary = "Customer Conformation: <br/>";

            summary += "Name: "+customer.CustomerName+ "<br/>";

            summary += "Phone Number: " + customer.CustomerPhone + "<br/>";

            summary += "Email: " + customer.CustomerEmail + "<br/>";

            summary += "Number of Pesrons: " + Services.numberofCustomer + "<br/>";

            summary += "Addtional Services: " + string.Join(" ",Services.additionalServices.ToArray()) + "<br/>";

            summary += "Type of room: " + Services.typeofRoom + "<br/>";

            summary += " Total amount is: " + CalculateAmount().ToString() + "<br/>";

            return summary;
        }

        public double CalculateAmount()
        {
            double total = 0;

            if (Services.typeofRoom == "S")
            {
                total = 100;
            }
            else if(Services.typeofRoom == "M")
            {
                total = 150;
            }
            else if(Services.typeofRoom == "D")
            {
                total = 200;
            }

            total += Services.additionalServices.Count() * 10;

            return total;
        }

    }

}
 
 