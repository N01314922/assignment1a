﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace N01314922_Assignment1
{
    public class Customer
    {
        private string customerName;
        private string customerPhone;
        private string customerEmail;
        private string customerAddress;

        public Customer()
        {

        }

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        public string CustomerPhone
        {
            get { return customerPhone; }
            set { customerPhone = value; }
        }

        public string CustomerEmail
        {
            get { return customerEmail; }
            set { customerEmail = value; }
        }

        public string CustomerAddress
        {
            get { return customerAddress; }
            set { customerAddress = value; }
        }
    }
}